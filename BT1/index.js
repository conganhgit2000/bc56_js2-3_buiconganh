/*
input:
    tienLuong
    soNgayLamViec
progress
    b1: khai báo biến chứa giá trị tiền lương và số ngày làm việc
    b2: cho tongLuong = soNgayLamViec * tienLuong
output:
    tongLuong    
 */


function tinhTongLuong() {
    var soNgayEl = document.getElementById('soNgayLamViec').value *1;
    var tienLuong = 100;
    
    var tongLuong = 0;
    tongLuong = soNgayEl * tienLuong;
    document.getElementById('tongLuong').innerHTML =`<h3 class ="mt-2 p-3 alert-warning"> ${tongLuong.toLocaleString() + ",000 VND" } </h3>`;
    
}