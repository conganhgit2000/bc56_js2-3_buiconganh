/*
input:
    kySo
progress
    b1: khai báo biến chứa giá trị kySo
        
    b2: cho phép tính
        hangChuc = Math.floor(kySoEl % 10)
        hangDonVi = Math.floor(kySoEl / 10)

output:
    kết quả của Tổng ký số
*/

function Tong() {
    var kySoEl = document.getElementById('kySo').value *1;

    var tongKySo = 0;

    var hangChuc = Math.floor(kySoEl / 10);
    var hangDonVi = Math.floor(kySoEl % 10);

    tongKySo = hangChuc + hangDonVi;
    
    document.getElementById('tongKySo').innerHTML = tongKySo;
}