/*
input:
    chieuDai
    chieuRong

progress
    b1: khai báo biến chứa giá trị chieuDai và chieuRong
        
    b2: cho phép tính
        diện tích : dienTich = chieuDaiEl * chieuRongEl
        chu vi : chuVi = (chieuDaiEl + chieuRongEl) * 2

output:
    kết quả của diện tích và chu vi
*/

function tinhKetQua() {
    var chieuDaiEl = document.getElementById('chieuDai').value *1;
    var chieuRongEl = document.getElementById('chieuRong').value *1;

    var dienTich = 0;
    var chuVi = 0;
    dienTich = chieuDaiEl * chieuRongEl;
    chuVi = (chieuDaiEl + chieuRongEl) * 2;

    // console.log(dienTich);
    // console.log(chuVi);

    document.getElementById('dienTich').innerHTML = dienTich;
    document.getElementById('chuVi').innerHTML = chuVi;

}